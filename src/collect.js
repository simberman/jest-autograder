const commandLineArgs = require('command-line-args');

const path = require('path');
const fs = require('fs/promises');
const axios = require('axios');
const util = require('util');
const exec = util.promisify(require('child_process').exec);

const jsdom = require('jsdom');

const optionDefinitions = [
    { name: 'dir', alias: 'd' },
];

const options = commandLineArgs(optionDefinitions);

const dir = path.resolve(options.dir);

console.log(dir);

const getDirectories = async source =>
  (await fs.readdir(source, { withFileTypes: true }))
    .filter(dirent => dirent.isDirectory())
    .map(dirent => dirent.name)

const getFiles = async source =>
    (await fs.readdir(source, { withFileTypes: true }))
      .filter(dirent => dirent.isFile())
      .map(dirent => dirent.name)

const dereferenceStudentFiles = (student) =>
{
    return new Promise( (resolve) => {

        const filePromises = [ getFiles(student.onlineTextDir) ]
        if(student.fileDir) {
            filePromises.push( getFiles(student.fileDir) );
        }

        Promise.all(filePromises).then( (listings) => {
                console.log(listings);
                resolve( { 
                    ...student,
                    onlineText: listings[0].map( l => path.join(student.onlineTextDir, l)),
                    files: listings[1] && listings[1].map( l => path.join(student.fileDir, l)),
                })
            }
        );        
    });
}

const getUrlsFromOnlinetext = (path) => {
    console.log(path);
    const re = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/g ;

    return fs.readFile(path, { encoding: 'utf8' })
        .then( contents => {
            let urls = [];
            const dom = new jsdom.JSDOM(contents);
            const document = dom.window.document;

            // First try to get URLs from links
            const links = document.querySelectorAll('a');
            urls = urls.concat( [...links].map( (a) => a.href ));

            // Next try to get any other URLs that may remain
            // Note that JSDOM doesn't have innerText, so I am banking
            // on only one URL per child node
            links.forEach( (l) => { l.remove() } );

            document.body.childNodes.forEach( (n) => {
                urls = urls.concat( n.textContent.match(re) || []);
            });   

            return urls;
        })    
};

const testOneStudent = async (student, testName) => {

    console.log(student)

    //console.log( `running cmd /C "set STUDENTID=${student.id} && set STUDENTNAME=${student.name} && set URL=${student.probableSonicLink} && npm run test ${testName}` );
    //return exec( `cmd /C "set STUDENTID=${student.id} && set STUDENTNAME=${student.name} && set URL=${student.probableSonicLink} && npm run test ${testName}`)
    
    probableFile = student.files && student.files.find( f => f.match( /\.html$/)) || "";

    console.log( `STUDENTID="${student.id}" STUDENTNAME="${student.name}" URL="${student.probableSonicLink}" SUBMITTEDFILEPATH="${probableFile}" npm run test ${testName}` );
    return exec( `STUDENTID="${student.id}" STUDENTNAME="${student.name}" URL="${student.probableSonicLink}" SUBMITTEDFILEPATH="${probableFile}" npm run test ${testName}`)
      .catch( (error) => {
        console.log( 'test contained failures');
      }).then( () => {
        console.log( 'copying ' + path.resolve( __dirname, '..', 'test-report.html') + ' to student directory');
        return fs.copyFile( path.resolve( __dirname, '..', 'test-report.html'),
                            path.join( student.onlineTextDir, 'test-report.html'));
      });
  };

getDirectories(dir).then(
    (dirlist) => {
        // Generate list of student IDs
        
        const splitList = dirlist.map( (dir) => dir.split('_'));

        const idList = Array.from(
            new Set( splitList.map( (splitList) => splitList[1] )).values())
            .map( (id) => {
                console.log(id);

                const onlineTextDir = path.join( dir, dirlist.find( (dir) => dir.search( `${id}.*onlinetext`) > -1));
                let fileDir = null;

                try {
                    fileDir = path.join( dir, dirlist.find( (dir) => dir.search( `${id}.*file`) > -1));
                } catch (e) {
                    
                }

                return { 
                    id,
                    name: splitList.find( (elt) => elt[1] == id)[0], // this is bad
                    onlineTextDir: onlineTextDir,
                    fileDir: fileDir
                }
            })
            ;
        return idList;
    }
).then( (students) => { 
    // Get all student files affiliated with a student
    return Promise.all( students.map( student => dereferenceStudentFiles(student)) )
        .then(  (students) => { console.log(students); return students; })

    }
).then( (students) => {
    // Get all link paths from inside the online text

    return Promise.all( 
        students.map( student => {

            return new Promise( (resolve) => {
                getUrlsFromOnlinetext(student.onlineText[0]).then( (links) => {
                    resolve( { ...student,
                        onlinetextUrls: links,
                        probableSonicLink: links.find( l => l.match(/^https:\/\/sonic/))                        
                    });
                });

            });
        })
    );
}).then( async (students) => {
    
    console.log(students);

    for(let i = 0; i < students.length; ++i) {
        let s = students[i];
        console.log(s.name, s.id, s.probableSonicLink);
        await testOneStudent(s, 'lab_wk6');
    }
  
});

