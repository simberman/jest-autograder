
const fs = require ('fs/promises');
const path = require('path');
const JSDOM = require('jsdom').JSDOM;
const neatCsv = require('neat-csv');


const GLITCH_NAME_RX = /(([a-z]+\-)+\-?)/g;
const MOODLE_ONLINETEXT_FILENAME = 'onlinetext.html';

const STUDENT_CSV_FILE = "C:\\Users\\Sam\\Dawson\ OneDrive\\OneDrive\ \-\ Dawson\ College\\Website\ Creation\ 2021\ Winter\\students.csv";

let cachedStudentData;

const generateGlitchUrl = (name) => {
  return `https://${name}.glitch.me/`;
};

const getOnlineTextContent = (url) => {
  return fs.readFile(url, {encoding: 'utf8'} )
    .then( (contents) => {
      dom = new JSDOM(contents, { runScripts: 'dangerously' });
      return dom.window.document.body.textContent;
    });
};

const uniformUrlFromOnlineText = (onlineText) => {
  const rxResult = onlineText.match(GLITCH_NAME_RX);
  if(!rxResult || !rxResult[0]) {
    return null;
  }
  return generateGlitchUrl(rxResult[0]);
};


const getStudentData = function() {
  if(cachedStudentData) return Promise.resolve(cached_student_data);
  else return fs.readFile(path.resolve(STUDENT_CSV_FILE))
    .then( (contents) => {
      cachedStudentData = neatCsv(contents, ['id', '', 'name', 'email', 'program', 'course', 'section']);
      return cachedStudentData;
    });
};

const getStudentIDFromMoodleName = function(name) {
  const nameTokens = name.split(' ');

  return getStudentData().then( (csvStudents) => {
    return csvStudents.find( (s) => s.name.indexOf(name[0]) > -1 && s.name.indexOf(name[name.length - 1]) > -1).id || null;
  });


};


const getStudentAssignmentData = function(dir) {

  return fs.readdir(path.resolve(dir))
    .then( (contents) => {
      return( contents.map( (c) => {
        const split = c.split('_');
        return {
          dirName: c,
          absolutePath: path.resolve(path.join(dir, c)),
          onlineTextPath: path.resolve(path.join(dir, c, MOODLE_ONLINETEXT_FILENAME)),
          name: split[0]
        };
      }));
    })
    .then( (arr) => {

      return Promise.all(
        arr.map( (student) => student.name)
          .map( (studentName) => getStudentIDFromMoodleName(studentName))
      ).then( ids => {
        return arr.map( (student, idx) => Object.assign( student, { id: ids[idx] } ));
      });
    })
    .then( (arr) => {
      return Promise.all(
        arr.map(
          (student) => {
            return new Promise( (resolve, reject) => {

              getOnlineTextContent(student.onlineTextPath)
                .then( (textContent) => {
                  resolve( Object.assign( student, {
                    textContent: textContent,
                    glitchUrl: textContent,
                    // glitchUrl: uniformUrlFromOnlineText(textContent)
                  } ));
                });
            });
          })
      );
    });
};

module.exports = {
  getStudentAssignmentData: getStudentAssignmentData,
  // getStudentData: getStudentData
};

// getStudentData().then( (data) => { console.log(data); });
