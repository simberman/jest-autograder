/**
 * @jest-environment puppeteer
 */

//import expect from 'expect-puppeteer'
import "html-validate/jest";

import expectPP from 'expect-puppeteer'
import axios from "axios";

import {promises as fs} from 'fs'; 

const studentId = process.env.STUDENTID || 'x01xxxx05';

const homogenize = (str) => str.replace(/\s\s+/g, ' ').trim().toLowerCase();
const _h = homogenize;

let rawSource;

describe(process.env.STUDENTNAME, () => {
  beforeAll(async () => {
    const response = await page.goto(process.env.URL, { waitUntil: 'domcontentloaded' });
    rawSource = await response.text();
  });

  // it('should put test in debug mode', async () => {
  //   await jestPuppeteer.debug();
  // });

  it('contains the same text as the submitted file', async () => {
    const fileContents = await fs.readFile(process.env.SUBMITTEDFILEPATH);
    expect(rawSource).toEqual(fileContents.toString());
  });

  it('validates (more stringent than official settings)', async () => {
    expect(rawSource).toHTMLValidate( { 
      extends: ['html-validate:standard'],
      rules: {
        'no-trailing-whitespace': 'off',

      },
      elements: [
        'html5',
        {
          th: {
            attributes: {
              "scope": { required: false }
            }
          }
        }
     ]
    });
  });

  describe( 'the table', () => {
    
    it( 'should exist', async () => {
      const table = await expectPP(page).toMatchElement('table');
    } );

    it( 'should have a thead, tbody, tfoot', async () => {
      const table = await expectPP(page).toMatchElement('table');

      const thead = await expectPP(page).toMatchElement('table>thead');
      const tbody = await expectPP(page).toMatchElement('table>tbody');
      const tfoot = await expectPP(page).toMatchElement('table>tfoot');
    } );

    it( 'should have 1 tr in the thead', async () => {
      await expectPP(page).toMatchElement('table>thead>tr');
      await expectPP(page).not.toMatchElement('table>thead>tr:nth-child(2)');
    });

    it( 'should have 5 th in the thead', async () => {
      await expectPP(page).toMatchElement('table>thead>tr>th:nth-child(5)');
    })

    it( 'should have at least 3 tr in the tbody', async () => {
      await expectPP(page).toMatchElement('table>tbody>tr:nth-child(3)');
    });

    it( 'should have hyperlinks in the second column that function', async () => {
      await expectPP(page).toMatchElement('table>tbody a');

      const links = await page.evaluate( () => {
        let elements = document.querySelectorAll( 'table>tbody a');
        return Array.from(elements).map(e => e.href);
      })
     
      // this mostly gets blocked by bots, oh well
      // const groceries = await browser.newPage();
      // let response;

      // for(let i = 0; i < links.length; ++i) {

      //   [response] = await Promise.all( [
      //     groceries.waitForNavigation(),
      //     groceries.goto(links[i])
      //   ]);
        
      //   expect(response.status()).toEqual(200);
      // }

      // await groceries.close();
    });

    it( 'should have several first cells with colspans', async () => {
      const colspanSolutions = [4, 3, 3, 2, 1];

      const colspans = await page.evaluate( () => {
        return Array.from(document.querySelectorAll('tr'))
          .slice(-5)
          .map(e => e.querySelector('td,th').colSpan);
      });

      expect(colspans).toEqual(colspanSolutions);
    });

    it( 'should have a last cell in the second to last row with colspan and rowspan', async () => {
      const tdValues = await page.evaluate( () => {
        const td = Array.from(document.querySelectorAll('tr'))
          .slice(-2, -1)
          [0]
          .querySelector("td:last-child");

        return { 
          class: td.className,
          id: td.id,
          colSpan: td.colSpan,
          rowSpan: td.rowSpan,
          backgroundColor: getComputedStyle(td).getPropertyValue('background-color')};
      });

      await page.hover( tdValues.id.length && `#${tdValues.id}` || `td.${tdValues.class}`);

      const tdHoverBgColor = await page.evaluate( () => {
        const td = Array.from(document.querySelectorAll('tr'))
          .slice(-2, -1)
          [0]
          .querySelector("td:last-child");

        return getComputedStyle(td).getPropertyValue('background-color');
      });

      expect(tdValues.colSpan).toEqual(2);
      expect(tdValues.rowSpan).toEqual(2);
      expect(tdValues.backgroundColor).not.toEqual(tdHoverBgColor);
    })
  });

  describe( 'the video', () => { 
    it( 'should exist', async () => {
      await expectPP(page).toMatchElement('video');
    });
    it( 'should have a source', async () => {
      await expectPP(page).toMatchElement('video source');
    });
    it( 'should have loaded', async () => {
      expect( await page.evaluate( () => {
        return document.querySelector('video').readyState;
       } )).toBeGreaterThanOrEqual(1);
    });
  });

  describe( 'the citation', () => {
    jest.setTimeout(10000);
    it( 'should be right after the table', async () => {
      await expectPP(page).toMatchElement('table+p');
    });
    it( 'should contain a link', async () => {
      await expectPP(page).toMatchElement('table+p a');
    });
    it( 'should properly go to a URL', async () => {

      const [response] = await Promise.all( [
        page.waitForNavigation(),
        page.click('table+p a')
      ]);
      expect(response.status()).toEqual(200);
      
      await Promise.all( [
        page.waitForNavigation(),
        page.goBack()
      ]);
      

    });



  });

  // it('should have a header "120 D&D" image', async () => {
  //   const aHref = await page.evaluate(() => document.querySelector('a').href);
  //   expect(aHref).toMatch(/characters.html/);

  //   const img = await page.evaluate(() => document.querySelector('a img'));
  //   expect(img).toBeDefined();

  //   const imgSrc = await page.evaluate(() => document.querySelector('a img').src);
  //   expect(imgSrc).toMatch(/logo.png/);
  // });

  // it('should have an h1 with a character name', async () => {
  //   const h1 = await page.evaluate(() => document.querySelector('h1')); 
  //   const h1Text = await page.evaluate(() => document.querySelector('h1').textContent);
  //   const numh1 = await page.evaluate(() => document.querySelectorAll('h1').length); 
  //   expect(h1).toBeDefined();
  //   expect(h1Text.length).toBeGreaterThan(0);
  //   expect(numh1).toEqual(1);
  // });

  // it('should have an h2 with a level', async () => {
  //   const h2 = await page.evaluate(() => document.querySelector('h2')); 
  //   const h2Text = await page.evaluate(() => document.querySelector('h2').textContent);
  //   const numh2 = await page.evaluate(() => document.querySelectorAll('h2').length); 
  //   expect(h2).toBeDefined();
  //   expect(h2Text.length).toBeGreaterThan(0);
  //   expect(h2Text).toMatch(/[lL]evel/g);
  //   expect(numh2).toEqual(1);
  // });

  // it('should have a paragraph with text in it', async () => {
  //   const p = await page.evaluate(() => document.querySelector('p')); 
  //   const pText = await page.evaluate(() => document.querySelector('p').textContent);
  //   const nump = await page.evaluate(() => document.querySelectorAll('p').length); 
  //   expect(p).toBeDefined();
  //   expect(pText.length).toBeGreaterThan(0);
  //   expect(nump).toEqual(1);
  // });

  // it('should have a figure with an image and figcaption in it', async () => {
  //   const figure = await page.evaluate(() => document.querySelector('figure')); 
  //   expect(figure).toBeDefined();

  //   const figureImg = await page.evaluate(() => document.querySelector('figure img')); 
  //   expect(figureImg).toBeDefined();
  //   expect(figureImg.alt).not.toEqual('');

  //   const figCaption= await page.evaluate(() => document.querySelector('figure figcaption')); 
  //   expect(figCaption).toBeDefined();


  // });

  // it('should have a ul with exactly 4 li', async () => {
  //   const ul = await page.evaluate(() => document.querySelector('ul')); 
  //   expect(ul).toBeDefined();

  //   const numli = await page.evaluate(() => document.querySelectorAll('li').length); 
  //   expect(numli).toEqual(4);
  //   const numlia = await page.evaluate(() => document.querySelectorAll('li a').length); 
  //   expect(numlia).toEqual(4);

  
  //   const liStyle = await page.evaluate(() => getComputedStyle(document.querySelector('li')));
  //   expect(liStyle.display).not.toBe('none');

  //   const liaBorderStyle = await page.evaluate(() => getComputedStyle(document.querySelector('li a')).getPropertyValue('border-style'));
  //   expect(liaBorderStyle).not.toBe('none');

  //   const aBackgroundColor = await page.evaluate( () => getComputedStyle(document.querySelector('li a')).getPropertyValue('background-color'));
  //   await page.hover('li a');
  //   await page.waitForSelector('li a:hover');
  //   const aHoverBackgroundColor = await page.evaluate( () => getComputedStyle(document.querySelector('li a:hover')).getPropertyValue('background-color'));
  //   expect(aBackgroundColor).not.toBe(aHoverBackgroundColor);
  //   await page.mouse.move(0,0);

  // });




})