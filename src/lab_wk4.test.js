/**
 * @jest-environment puppeteer
 */

//import expect from 'expect-puppeteer'
import "html-validate/jest";

import expectPuppeteer from 'expect-puppeteer'

const studentId = process.env.STUDENTID || 'x01xxxx05';

const homogenize = (str) => str.replace(/\s\s+/g, ' ').trim().toLowerCase();
const _h = homogenize;

describe(process.env.STUDENTNAME, () => {
  beforeAll(async () => {
    await page.goto(process.env.URL, { waitUntil: 'domcontentloaded' });
  });

  // it('should put test in debug mode', async () => {
  //   await jestPuppeteer.debug();
  // });

  it('validates (more stringent than official settings)', async () => {
    const pageContent = await page.content(); 
    // This doesn't work because it pulls in processed harmonized DOM
    // Need to get the original source as it was downloaded

    expect(pageContent).toHTMLValidate( { 
      rules: {
        'no-trailing-whitespace': 'off'
      }
    });
  });

  it('should have a header "120 D&D" image', async () => {
    const aHref = await page.evaluate(() => document.querySelector('a').href);
    expect(aHref).toMatch(/characters.html/);

    const img = await page.evaluate(() => document.querySelector('a img'));
    expect(img).toBeDefined();

    const imgSrc = await page.evaluate(() => document.querySelector('a img').src);
    expect(imgSrc).toMatch(/logo.png/);
  });

  it('should have an h1 with a character name', async () => {
    const h1 = await page.evaluate(() => document.querySelector('h1')); 
    const h1Text = await page.evaluate(() => document.querySelector('h1').textContent);
    const numh1 = await page.evaluate(() => document.querySelectorAll('h1').length); 
    expect(h1).toBeDefined();
    expect(h1Text.length).toBeGreaterThan(0);
    expect(numh1).toEqual(1);
  });

  it('should have an h2 with a level', async () => {
    const h2 = await page.evaluate(() => document.querySelector('h2')); 
    const h2Text = await page.evaluate(() => document.querySelector('h2').textContent);
    const numh2 = await page.evaluate(() => document.querySelectorAll('h2').length); 
    expect(h2).toBeDefined();
    expect(h2Text.length).toBeGreaterThan(0);
    expect(h2Text).toMatch(/[lL]evel/g);
    expect(numh2).toEqual(1);
  });

  it('should have a paragraph with text in it', async () => {
    const p = await page.evaluate(() => document.querySelector('p')); 
    const pText = await page.evaluate(() => document.querySelector('p').textContent);
    const nump = await page.evaluate(() => document.querySelectorAll('p').length); 
    expect(p).toBeDefined();
    expect(pText.length).toBeGreaterThan(0);
    expect(nump).toEqual(1);
  });

  it('should have a figure with an image and figcaption in it', async () => {
    const figure = await page.evaluate(() => document.querySelector('figure')); 
    expect(figure).toBeDefined();

    const figureImg = await page.evaluate(() => document.querySelector('figure img')); 
    expect(figureImg).toBeDefined();
    expect(figureImg.alt).not.toEqual('');

    const figCaption= await page.evaluate(() => document.querySelector('figure figcaption')); 
    expect(figCaption).toBeDefined();


  });

  it('should have a ul with exactly 4 li', async () => {
    const ul = await page.evaluate(() => document.querySelector('ul')); 
    expect(ul).toBeDefined();

    const numli = await page.evaluate(() => document.querySelectorAll('li').length); 
    expect(numli).toEqual(4);
    const numlia = await page.evaluate(() => document.querySelectorAll('li a').length); 
    expect(numlia).toEqual(4);

  
    const liStyle = await page.evaluate(() => getComputedStyle(document.querySelector('li')));
    expect(liStyle.display).not.toBe('none');

    const liaBorderStyle = await page.evaluate(() => getComputedStyle(document.querySelector('li a')).getPropertyValue('border-style'));
    expect(liaBorderStyle).not.toBe('none');

    const aBackgroundColor = await page.evaluate( () => getComputedStyle(document.querySelector('li a')).getPropertyValue('background-color'));
    await page.hover('li a');
    await page.waitForSelector('li a:hover');
    const aHoverBackgroundColor = await page.evaluate( () => getComputedStyle(document.querySelector('li a:hover')).getPropertyValue('background-color'));
    expect(aBackgroundColor).not.toBe(aHoverBackgroundColor);
    await page.mouse.move(0,0);

  });




})