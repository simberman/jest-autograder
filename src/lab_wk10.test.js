/**
 * @jest-environment puppeteer
 */

//import expect from 'expect-puppeteer'
import "html-validate/jest";

import expectPP from 'expect-puppeteer'
import axios from "axios";

import {promises as fs} from 'fs'; 

const studentId = process.env.STUDENTID || 'x01xxxx05';

const homogenize = (str) => str.replace(/\s\s+/g, ' ').trim().toLowerCase();
const _h = homogenize;

let rawSource;

describe(process.env.STUDENTNAME, () => {
  beforeAll(async () => {
    const response = await page.goto(process.env.URL, { waitUntil: 'domcontentloaded' });
    rawSource = await response.text();
  });

  // it('should put test in debug mode', async () => {
  //   await jestPuppeteer.debug();
  // });

  it('contains the same text as the submitted file', async () => {
    const fileContents = await fs.readFile(process.env.SUBMITTEDFILEPATH);
    expect(rawSource).toEqual(fileContents.toString());
  });

  it('validates (more stringent than official settings)', async () => {
    expect(rawSource).toHTMLValidate( { 
      extends: ['html-validate:standard'],
      rules: {
        'no-trailing-whitespace': 'off',

      },
      elements: [
        'html5',
        {
          th: {
            attributes: {
              "scope": { required: false }
            }
          }
        }
     ]
    });
  });

  describe( 'the form is ', () => {

    it( 'has a grid layout in the form', async () => {
      // li: display inline-block
      // a: color: white, padding: 10px, font-size: 2px, no text-decoration

      await expectPP(page).toMatchElement("form img:nth-child(1)");
      await expectPP(page).toMatchElement("form label:nth-child(2)");
      await expectPP(page).toMatchElement("form input:nth-child(3)");
      await expectPP(page).toMatchElement("form label:nth-child(4)");
      await expectPP(page).toMatchElement("form input:nth-child(5)");
      await expectPP(page).toMatchElement("form label:nth-child(6)");
      await expectPP(page).toMatchElement("form input:nth-child(7)");
      await expectPP(page).toMatchElement("form button:nth-child(8)");
      await expectPP(page).toMatchElement("form div#answer:nth-child(9)");



    });

    const testcases = [
      {radius: 1, height: 1, scoops: 1, messageContains: "5.24"},
      {radius: 2.5, height: 2.5, scoops: 2, messageContains: "147"},
      {radius: 1.75, height: 2, scoops: 3, messageContains: "73.8"},
      {radius: 3, height: 5, scoops: 5, messageContains: "613"},
      {radius: 2.5, height: 2.5, scoops: 2.5, messageContains: "full scoops"},
      {radius: -1, height: 1, scoops: 1, messageContains: "less than 1"},
      {radius: 0, height: 1, scoops: 1, messageContains: "less than 1"},
      {radius: 1, height: 0, scoops: 1, messageContains: "less than 1"},
      {radius: 1, height: 1, scoops: 0, messageContains: "less than 1"},
      {radius: 1, height: 1, scoops: 1.5, messageContains: "full scoops"}
    ];

    for(let c of testcases) {
      it(`r = ${c.radius}, h = ${c.height}, s = ${c.scoops} contains ${c.messageContains}`, async () => {
        await expectPP(page).toFill("#radius", String(c.radius));
        await expectPP(page).toFill("#height", String(c.height));
        await expectPP(page).toFill("#scoops", String(c.scoops));
        await expectPP(page).toClick("button");
        await expectPP(page).toMatchElement("#answer", {text: c.messageContains})
      });
    }
  });

});

