import { fireEvent, getByText } from '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';
import { JSDOM } from 'jsdom';
import { promises as fs } from 'fs';
import path from 'path';

import process from 'process';

// const html = fs.readFileSync(path.resolve(__dirname, './index.html'), 'utf8');

const TITLES = [
  "Dragon's Heir",
  "Queens's Tribute",
  "Lure the Truth",
  "Point of Steel",
  "Claw of Wolves",
  "The Last Marigold",
  "Paradise Bound",
  "Iris Beauty",
  "End of Solaris",
  "Gates of Legend" ];

const AUTHORS = [
  "Addison Hardy",
  "Bailey Martin",
  "Cameron Miles",
  "Darby Walker",
  "Hunter Green",
  "Indigo Blue",
  "Jordan Gibbs",
  "Kai Tucker",
  "Lee Sotto",
  "Pat Blake" ];

const PART1_CONTENT = [
  "He had reached the point where he was paranoid about being paranoid...",
  "She knew it was going to be a bad day when he saw mountain lions roaming the streets...",
  "They did nothing as the raccoon attacked the kid’s lunchbox...",
  "Tomorrow will bring something new, so leave today as a memory...",
  "That was how she came to win a cool $1 million...",
  "The fog was so dense even a laser decided it wasn't worth the effort...",
  "Separation anxiety is what happens when you can't find your phone...",
  "She uses onomatopoeia as a weapon of mental destruction...",
  "The band improved dramatically once the lead singer left for...",
  "It was getting dark, and we weren’t even close to our destination..."
];

const PART2_CONTENT = [
  "He had reached the point where he was paranoid about being paranoid...",
  "She knew it was going to be a bad day when he saw mountain lions roaming the streets...",
  "They did nothing as the raccoon attacked the kid’s lunchbox...",
  "Tomorrow will bring something new, so leave today as a memory...",
  "That was how she came to win a cool $1 million...",
  "The fog was so dense even a laser decided it wasn't worth the effort...",
  "Separation anxiety is what happens when you can't find your phone...",
  "She uses onomatopoeia as a weapon of mental destruction...",
  "The band improved dramatically once the lead singer left for...",
  "It was getting dark, and we weren’t even close to our destination..."
];


let dom;
let head;
let body;


const studentId = process.env.STUDENTID || 'x01xxxx05';

const homogenize = (str) => str.replace(/\s\s+/g, ' ').trim().toLowerCase();
const _h = homogenize;

describe(process.env.STUDENTNAME, () => {
  beforeAll(() => {
    return fs.readFile(path.resolve(__dirname, './lab0.html'), { encoding: 'utf8' })
      .then( (contents) => {
        dom = new JSDOM(contents, { runScripts: 'dangerously' });
        head = dom.window.document.head;
        body = dom.window.document.body;
      });
  });

  describe('the head element', () => {
    it('has a title "Lab 0"', () => {
      expect(head.querySelector('title')).toBeInTheDocument();
      expect(head.querySelector('title').textContent).toEqual('Lab 0');
    });

    it('has a meta tag which specifies the UTF-8 charset', () => {
      expect(head.querySelector('meta[charset]')).toBeInTheDocument();
      expect(_h(head.querySelector('meta[charset]').getAttribute('charset'))).toEqual('utf-8');
    });

  });

  describe('the body element', () => {

    describe('order of tags', () => {
      const tagOrder = ['h1', 'p', 'hr', 'h2', 'p', 'h2', 'p'];
      for(let t = 0; t < tagOrder.length; ++t) {
        it(`tag ${t} is ${tagOrder[t]}`, () => {
          expect( _h(body.children[t].tagName)).toEqual(tagOrder[t]);
        });
      }
    });

    describe('the title block area', () => {
      // const firstH1 = body.querySelector('h1');

      it( 'has a h1 tag with a valid title as content', () => {
        expect(body.querySelector('h1')).toBeInTheDocument();
        expect(TITLES.map( t => _h(t)))
          .toContain(
            _h(body.querySelector('h1').textContent));
      });

      it( 'contains the correct title for the last number of the student ID', () => {
        expect( _h(body.querySelector('h1').textContent))
          .toEqual( _h(TITLES[ studentId[studentId.length - 1] ]));
      });

      it( 'contains a p tag after the h1 tag, with a strong tag in it', () => {
        expect( body.querySelector('h1+p strong')).toBeInTheDocument();
      });

      it( 'contains the correct author', () => {
        expect( _h(body.querySelector('h1+p strong').textContent) )
          .toEqual(_h(AUTHORS[ studentId[studentId.length - 2] ]));
      });

      it( 'contains a hr after all that', () => {
        expect( body.querySelector('h1+p+hr')).toBeInTheDocument();
      });
    });

    describe('the content section', () => {
      it( 'has "Part 1" in the first h2 tag', () => {
        expect( _h(body.querySelectorAll('h2')[0].textContent)).toEqual('part 1');
      });

      it( 'contains the correct text in Part 1', () => {
        expect( _h(body.querySelectorAll('h2+p')[0].textContent))
          .toEqual(_h(PART1_CONTENT[ studentId[1]]));
      });

      it( 'has "Part 2" in the second h2 tag', () => {
        expect( _h(body.querySelectorAll('h2')[1].textContent)).toEqual('part 2');
      });

      it( 'contains the correct text in Part 2', () => {
        expect( _h(body.querySelectorAll('h2+p')[1].textContent))
          .toEqual( _h(PART1_CONTENT[ studentId[2]]));
      });
    });
  });

});
