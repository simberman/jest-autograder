const commandLineArgs = require('command-line-args');
const parseDir = require('./parsedir.js');
const path = require('path');
const fs = require('fs/promises');
const axios = require('axios');
const util = require('util');
const exec = util.promisify(require('child_process').exec);

const optionDefinitions = [
  { name: 'dir', alias: 'd' },
  { name: 'testname', alias: 't' },
  { name: 'simulate' }
];

const TEST_DIR = path.resolve('C:\\Users\\Sam\\Downloads\\Web Site Creation Fall 2020-Lab 0 Your First Web Page-226158');


const options = commandLineArgs(optionDefinitions);

const dir = path.resolve(options.dir);
const testName = options.testname;
console.log(dir, testName);

console.log(__dirname);

const testOneStudent = (student) => {

  console.log('rm ' + path.resolve( path.join( __dirname, testName + '.html')));
  return fs.rm( path.resolve( path.join( __dirname, testName + '.html')), { force: true })
    .then( () => {
      console.log('axios get ' + student.glitchUrl);
      return axios.get( student.glitchUrl);
    }).then( (response) => {
      console.log ('writing to ' + path.join( __dirname, testName + '.html'));
      return fs.writeFile( path.join( __dirname, testName + '.html'), response.data);
    }).then( () => {
      console.log( `running cmd /C "set STUDENTID=${student.id} && set STUDENTNAME=${student.name} && npm test ${testName}` );
      return exec( `cmd /C "set STUDENTID=${student.id} && set STUDENTNAME=${student.name} && npm test ${testName}`);
    }).catch( (error) => {
      console.log( 'test contained failures');
    }).then( () => {
      console.log( 'copying ' + path.join( __dirname, testName + '.html') + ' to student directory');
      return fs.copyFile( path.join( __dirname, testName + '.html'),
                          path.join( student.absolutePath, testName + '.html'));
    }).then( () => {
      console.log( 'copying ' + path.resolve( __dirname, '..', 'test-report.html') + ' to student directory');
      return fs.copyFile( path.resolve( __dirname, '..', 'test-report.html'),
                          path.join( student.absolutePath, 'test-report.html'));
    });
};


studentData = parseDir.getStudentAssignmentData(dir)
  .then( (students) => {
    console.log(students[0]);
    testOneStudent(students[0]);
    testOneStudent(students[1]);

  });



