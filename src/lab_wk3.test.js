import { fireEvent, getByText } from '@testing-library/dom';
import '@testing-library/jest-dom/extend-expect';

import { JSDOM } from 'jsdom';
import { promises as fs } from 'fs';
import path from 'path';

import process from 'process';
import { w3cHtmlValidator } from 'w3c-html-validator';
import "html-validate/jest";

// const html = fs.readFileSync(path.resolve(__dirname, './index.html'), 'utf8');

let dom, htmlPath, rawHtml;
let head;
let body;

const studentId = process.env.STUDENTID || 'x01xxxx05';

const homogenize = (str) => str.replace(/\s\s+/g, ' ').trim().toLowerCase();
const _h = homogenize;

describe(process.env.STUDENTNAME, () => {
  beforeAll(() => {
    htmlPath = path.resolve(__dirname, './labwk3.html');
    return fs.readFile(htmlPath, { encoding: 'utf8' })
      .then( (contents) => {
        dom = new JSDOM(contents, { runScripts: 'dangerously' });
        rawHtml = contents;
        head = dom.window.document.head;
        body = dom.window.document.body;
      });
  });

  describe('the page', () => {
    it('validates', () => {
      return w3cHtmlValidator
        .validate( { html: rawHtml })
        .then( (results) => {
            expect(results.status).toEqual(200);
            expect(results.validates).toBeTruthy();
        });
    });

    it('validates (more stringent than official settings)', () => {
      expect(rawHtml).toHTMLValidate();
    });
  });

  describe('the head element', () => {
    it('has a title', () => {
      expect(head.querySelector('title')).toBeInTheDocument();
    });

    it('has a meta tag which specifies the UTF-8 charset', () => {
      expect(head.querySelector('meta[charset]')).toBeInTheDocument();
      expect(_h(head.querySelector('meta[charset]').getAttribute('charset'))).toEqual('utf-8');
    });

  });

  describe('the body element', () => {

    describe('order of tags', () => {
      const tagOrder = ['h1', 'p', 'hr', 'h2', 'ul', 'hr', 'h2', 'ol', 'hr', 'h3', 'dl', 'hr', 'p'];
      for(let t = 0; t < tagOrder.length; ++t) {
        it(`tag ${t} is ${tagOrder[t]}`, () => {
          expect( _h(body.children[t].tagName)).toEqual(tagOrder[t]);
        });
      }
    });

    describe('the title block area', () => {
      // const firstH1 = body.querySelector('h1');

      it( 'has a h1 tag with text the same as the title', () => {
        expect(body.querySelector('h1')).toBeInTheDocument();
        expect(_h(head.querySelector('title').textContent))
          .toEqual(_h(body.querySelector('h1').textContent));
      });

      it( 'contains a p tag after the h1 tag, with 2 strong tags in it', () => {
        expect( body.querySelector('h1+p strong:nth-of-type(2)')).toBeInTheDocument();
        expect( body.querySelector('h1+p strong:nth-of-type(3)')).not.toBeInTheDocument();
      });
      it( 'contains a hr after all that', () => {
        expect( body.querySelector('h1+p+hr')).toBeInTheDocument();
      });
    });

    describe('the ingredients section', () => {
      it( 'has a h2 tag that is the first in the document and reads "Ingredients"', () =>{ 
        expect(_h(body.querySelector('body>h2:nth-of-type(1)').textContent))
          .toEqual(_h('Ingredients'));
      });
      it( 'has a ul after the first h2 in the document that contains many li', () => {
        expect( body.querySelector('body>h2:nth-of-type(1)+ul li:nth-of-type(2)')).toBeInTheDocument();
      });
    });

    describe('the directions section', () => {
      it( 'has a h2 tag that is the second in the document and reads "Directions"', () =>{ 
        expect(_h(body.querySelector('body>h2:nth-of-type(2)').textContent))
          .toEqual(_h('Directions'));
      });
      it( 'has a ol after the first h2 in the document that contains many li', () => {
        expect( body.querySelector('body>h2:nth-of-type(2)+ol li:nth-of-type(2)')).toBeInTheDocument();
      });
    });

    describe('the nutrition facts section', () => {
      it( 'has a h3 tag that is the first in the document and reads "Nutrition Facts"', () =>{ 
        expect(_h(body.querySelector('body>h3:nth-of-type(1)').textContent))
          .toEqual(_h('Nutrition Facts'));
      });
      it( 'has a dl after the first h3 in the document that contains many dt and dd', () => {
        expect( body.querySelector('body>h3:nth-of-type(1)+dl dt:nth-of-type(2)+dd')).toBeInTheDocument();
      });
      it( 'has several dd and dt, and every dt has at least one dd associated with it', () => {
        expect( body.querySelector('body>h3:nth-of-type(1)+dl dt+:not(dd)')).not.toBeInTheDocument();
      });
      it( 'has nothing but dt and dd inside the dl', () => {
        expect( body.querySelector('body>h3:nth-of-type(1)+dl :not(dt,dd)')).not.toBeInTheDocument();
      });
    });

    // describe('the content section', () => {
    //   it( 'has "Part 1" in the first h2 tag', () => {
    //     expect( _h(body.querySelectorAll('h2')[0].textContent)).toEqual('part 1');
    //   });

    //   it( 'contains the correct text in Part 1', () => {
    //     expect( _h(body.querySelectorAll('h2+p')[0].textContent))
    //       .toEqual(_h(PART1_CONTENT[ studentId[1]]));
    //   });

    //   it( 'has "Part 2" in the second h2 tag', () => {
    //     expect( _h(body.querySelectorAll('h2')[1].textContent)).toEqual('part 2');
    //   });

    //   it( 'contains the correct text in Part 2', () => {
    //     expect( _h(body.querySelectorAll('h2+p')[1].textContent))
    //       .toEqual( _h(PART1_CONTENT[ studentId[2]]));
    //   });
    // });
  });

});
