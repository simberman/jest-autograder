/**
 * @jest-environment puppeteer
 */

//import expect from 'expect-puppeteer'
import "html-validate/jest";

import expectPP from 'expect-puppeteer'
import axios from "axios";

import {promises as fs} from 'fs'; 

const studentId = process.env.STUDENTID || 'x01xxxx05';

const homogenize = (str) => str.replace(/\s\s+/g, ' ').trim().toLowerCase();
const _h = homogenize;

let rawSource;

describe(process.env.STUDENTNAME, () => {
  beforeAll(async () => {
    const response = await page.goto(process.env.URL, { waitUntil: 'domcontentloaded' });
    rawSource = await response.text();
  });

  // it('should put test in debug mode', async () => {
  //   await jestPuppeteer.debug();
  // });

  it('contains the same text as the submitted file', async () => {
    const fileContents = await fs.readFile(process.env.SUBMITTEDFILEPATH);
    expect(rawSource).toEqual(fileContents.toString());
  });

  it('validates (more stringent than official settings)', async () => {
    expect(rawSource).toHTMLValidate( { 
      extends: ['html-validate:standard'],
      rules: {
        'no-trailing-whitespace': 'off',

      },
      elements: [
        'html5',
        {
          th: {
            attributes: {
              "scope": { required: false }
            }
          }
        }
     ]
    });
  });

  describe( 'the header', () => {
    it( 'should be styled', async () => {
      // height: 150px
      // h1: line height: 140px, font-size: 60px;

      const header = await expectPP(page).toMatchElement('header');
      const h1 = await expectPP(page).toMatchElement('header>h1');

      const styles = await page.evaluate( () => {
        const header = document.querySelector('header');
        const h1 = document.querySelector('header>h1');

        return( {
          headerHeight: getComputedStyle(header).getPropertyValue('height'),
          h1LineHeight: getComputedStyle(h1).getPropertyValue('line-height'),
          h1FontSize: getComputedStyle(h1).getPropertyValue('font-size')
        });
      });

      expect(styles.headerHeight).toEqual("150px");
      expect(styles.h1LineHeight).toEqual("140px");
      expect(styles.h1FontSize).toEqual("60px");
    });

    it( 'has a navbar that is properly styled', async () => {
      // li: display inline-block
      // a: color: white, padding: 10px, font-size: 2px, no text-decoration

      const nav = await expectPP(page).toMatchElement('nav');
      const secondA = await expectPP(page).toMatchElement('nav li:nth-child(2)>a');

      const styles = await page.evaluate( () => {
        const nav = document.querySelector('nav');
        const secondA = document.querySelector('nav li:nth-child(2)>a');

        return( {
          navDisplay: getComputedStyle(nav).getPropertyValue('display'),
          secondAColor: getComputedStyle(secondA).getPropertyValue('color'),
          secondAPadding: getComputedStyle(secondA).getPropertyValue('padding-top'),
          secondATextDecoration: getComputedStyle(secondA).getPropertyValue('text-decoration-line'),
          secondABackgroundColor: getComputedStyle(secondA).getPropertyValue('background-color')
        });
      });

      expect(styles.navDisplay).toEqual("block");
      expect(styles.secondAColor).toEqual("rgb(255, 255, 255)");
      expect(styles.secondAPadding).toEqual("10px");
      expect(styles.secondATextDecoration).toEqual("none");

      await page.hover('nav li:nth-child(2)>a');

      const hoverStyles = await page.evaluate( () => {
        const secondA = document.querySelector('nav li:nth-child(2)>a');

        return( {
          secondABackgroundColor: getComputedStyle(secondA).getPropertyValue('background-color')
        });
      });

      expect(styles.secondABackgroundColor).not.toEqual(hoverStyles.secondABackgroundColor);

    });

    it( 'has an absolutely positioned image', async () => {
      // position: absolute
      // right: 30px;
      //top: 30px;

      const img = await expectPP(page).toMatchElement('header>img');

      const styles = await page.evaluate( () => {
        const img = document.querySelector('header>img');

        return( {
          position: getComputedStyle(img).getPropertyValue('position'),
          right: getComputedStyle(img).getPropertyValue('right'),
          top: getComputedStyle(img).getPropertyValue('top')
        });
      });

      expect(styles.position).toEqual("absolute");
      expect(styles.right).toEqual("30px");
      expect(styles.top).toEqual("30px"); 

    });
  })

  describe( 'the flex layout', () => {
    // #layout has display: flex;
    // #layout> main has flex: 1
    // #layout > section has width: 350px and display:flex and flex-direction: column and justify-content: flex-start;

    // has a section with an h2 that has the text "speakers"

    it( 'is properly set up with a flex on the left and width on the right', async () => {
      const layout = await expectPP(page).toMatchElement('#layout');
      const main = await expectPP(page).toMatchElement('#layout>main');
      const section = await expectPP(page).toMatchElement('#layout>section');


      const styles = await page.evaluate( () => {
        const layout = document.querySelector('#layout');
        const main = document.querySelector('#layout>main');
        const section = document.querySelector('#layout>section');

        return( {
          layoutDisplay: getComputedStyle(layout).getPropertyValue('display'),
          mainFlex: getComputedStyle(main).getPropertyValue('flex-grow'),
          sectionWidth: getComputedStyle(section).getPropertyValue('width')
        });
      });

      expect(styles.layoutDisplay).toEqual("flex");
      expect(styles.mainFlex).toEqual("1");
      expect(styles.sectionWidth).toEqual("350px");      

    });
  });

  describe ('the links to other pages', () => {
    it( 'works', async () => {
      let responseA, responseB, responseC;
      await expectPP(page).toMatchElement('nav li:nth-child(2)');

      [responseA] = await Promise.all( [
        page.waitForNavigation(),
        page.click('nav li:nth-child(2)>a')
      ]);
      await page.goBack();

      [responseB] = await Promise.all( [
        page.waitForNavigation(),
        page.click('nav li:nth-child(3)>a')
      ]); 
      await page.goBack();      

      [responseC] = await Promise.all( [
        page.waitForNavigation({waitUntil: 'domcontentloaded'}),
        page.waitForSelector('button'),
        page.click('nav li:nth-child(4)>a')
      ]);

      expect(responseA.status()).toEqual(200);
      expect(responseB.status()).toEqual(200);
      expect(responseC.status()).toEqual(200);
    });
  });

  describe ('the order form page', () => {

    it( 'has a form', async () => {
      await expectPP(page).toMatchElement('form');
    });

    it( 'has a select \'mealtype\'', async () => {
      const select = await expectPP(page).toMatchElement('select[name="mealtype"]', '');

      const options = await page.evaluate(element => 
        Array.from(element.querySelectorAll('option')).map(o => o.value)
      , select);

      await expectPP(page).toSelect('select[name="mealtype"]', options[2]);

    });

    it( 'has an input \'amount\'', async () => {
      const input = await expectPP(page).toMatchElement('input[name="amount"]');
      const attrs = await page.evaluate(element => { return {
        max: element.max,
        min: element.min,
        required: element.required }; }, input);

      expect(attrs.max).toEqual("12");
      expect(attrs.min).toEqual("1");
      expect(attrs.required).toBeTruthy();

      await expectPP(page).toMatchElement('input#amount');
      await expectPP(page).toMatchElement('label[for="amount"'); 
    });

    it( 'has a textarea \'comments\'', async () => {
      const input = await expectPP(page).toMatchElement('textarea[name="comments"]');
      const attrs = await page.evaluate(element => { return {
        required: element.required }; }, input);

      expect(attrs.required).toBeFalsy();

      await expectPP(page).toMatchElement('textarea#comments');
      await expectPP(page).toMatchElement('label[for="comments"'); 
    });

    it( 'has an input \'first\'', async () => {
      const input = await expectPP(page).toMatchElement('input[name="first"]');
      const attrs = await page.evaluate(element => { return {
        required: element.required }; }, input);

      expect(attrs.required).toBeTruthy();

      await expectPP(page).toMatchElement('input#first');
      await expectPP(page).toMatchElement('label[for="first"'); 
    });

    it( 'has an input \'last\'', async () => {
      const input = await expectPP(page).toMatchElement('input[name="last"]');
      const attrs = await page.evaluate(element => { return {
        required: element.required }; }, input);

      expect(attrs.required).toBeTruthy();

      await expectPP(page).toMatchElement('input#last');
      await expectPP(page).toMatchElement('label[for="last"'); 
    });

    it( 'has an input \'email\'', async () => {
      const input = await expectPP(page).toMatchElement('input[name="email"]');
      const attrs = await page.evaluate(element => { return {
        required: element.required }; }, input);

      expect(attrs.required).toBeTruthy();
    });


  });

  describe('the form submission', () => {
    jest.setTimeout(15000);
    it( 'works', async () => {
      const formSub = {
        first: "Sam",
        last: "Imberman",
        amount: "10",
        comments: 'This is a super great comment!',
        email: 'simberman@dawsoncollege.qc.ca'
      };

      await expectPP(page).toMatchElement('form');

      for(let k of Object.keys(formSub)) {
        await expectPP(page).toFill(`[name="${k}"]`, formSub[k]);

      }

      let response;
      [response] = await Promise.all( [
        page.waitForNavigation({waitUntil: 'domcontentloaded'}),
        page.click('button')
      ]);
      expect(response.status()).toEqual(200);

      for(let k of Object.keys(formSub)) {
        let elt = await expectPP(page).toMatchElement(`#${k}`);
        let text = await page.evaluate(element => element.innerText, elt);

        expect(text).toEqual(formSub[k]);
      }
    })


  });



});

