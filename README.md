
# Autograder for Lab 0
Trying to use Jest etc to automatically grade Lab 0.
*Do not share this code too widely as it will bring shame upon my family.*

# DOM Testing Demo

[Based on this](https://levelup.gitconnected.com/how-to-unit-test-html-and-vanilla-javascript-without-a-ui-framework-c4c89c9f5e56?source=friends_link&sk=15836e19406fd422a13411e3ef497c0d) about this project as well.

# Seriously this is pretty sketchy

It downloads a Glitch URL, copies it to the project's `src` directory, and then runs a jest unit test against it. There is a constant where you can put a file path to a CSV containing student data, which needs the same columns as the Excel file you downloaded off Omnivox.

# How to use it

Assuming that we are using the Lab 0 
`npm run start -- --dir "C:\Users\Sam\Downloads\Web Site Creation Sections 6, 7 - Winter 2021-Lab 0 Your First Web Page-313510" --testname lab0`

* `--dir` is a directory corresponding to an unzipped download of Moodle "online text", where the contents are raw URLs. No error checking is performed on those and they are obtained with a very rudimentary regex that will break.
* `--testname` is the name of the unit test to run. If you run with `--testname lab0`, it will copy the downloaded Glitch URL to `lab0.html` and run `lab0.test.js` against it.
